import React from 'react';
import { useLocalStorage} from "./helpers/useLocalStorage";
import NavState from './components/menu/context/navState';
import Footer from './components/Footer/Footer';
import {sendRequest} from "./helpers/sendRequest";
import {useEffect, useState} from "react";
import ModalImage from './components/Modal/ModalImage/ModalImage';
import AppRoutes from './components/AppRoutes';
function App() {
	const [productArray, setProductArray] = useState([])
	useEffect(() => {
		sendRequest(window.location.origin +'/products.json')
			.then((date) => {	
				setProductArray(date);
			})
		}, []);

	const [carts, setCart] = useLocalStorage("CookiCart", "");
	const [favorites, setFavorite] = useLocalStorage("CookiFavorite", "");

	const handleFavorites = (products) => {
		const isAdded = favorites.some((favorite)=> favorite.article === products.article)
		if(isAdded){
			setFavorite(favorites.filter((favorite)=> favorite.article !== products.article));
			return
		}
		setFavorite([...favorites,products])
	}

	const handleCart = (products) => {
		const isAdded = carts.some((cart)=> cart.article === products.article)
		isAdded ?
				setCart(carts.filter((cart)=> cart.article !== products.article))
				:
				setCart([...carts,products]);
	}
	const [isModalImage,setIsModalImage] = useState(false)
	const handleModalImage = () => setIsModalImage(!isModalImage)

	const [currentPost, setCurrentPost] = useState({})
	const handleCurrentPost = (cardPost) => setCurrentPost(cardPost)
	
  return (
  <>
     <NavState countFavorite={favorites.length} countCart={carts.length}/>
	<AppRoutes 	favorite={favorites}
				productArray={productArray}
				carts={carts}
				handleModal={handleModalImage}
				handleCurrentPost={handleCurrentPost}
				onClickIcon={handleFavorites}/>
	<Footer/>
	<ModalImage
                isOpen={isModalImage}
                handleClose={()=>{handleModalImage()}}
				products={currentPost}
				cart={carts}
				handleOk={()=> {
						handleCart(currentPost)
						}}
    />
</>
  );
}
export default App;

