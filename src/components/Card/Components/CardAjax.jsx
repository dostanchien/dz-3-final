import PropTypes from 'prop-types'
import styled from 'styled-components';

const DivSt =styled.div`
text-align: left;
font-weight:400;
font-style: italic;
padding-top: 0;
`;

const CardAjax = ({children})=>{
    return(
        <DivSt><p>{children}</p></DivSt>
    )
}

CardAjax.propTypes = {
    children: PropTypes.any
}

export default CardAjax