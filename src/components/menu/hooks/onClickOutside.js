/* И последняя вспомогательная функция, когда пользователь кликает мышью вне навбара/меню, меню должно автоматически скрываться.
Как это сделать? В React’е есть хук useRef(), который позволяет обращаться напрямую к DOM. Таким образом, при клике можно проверять,
попадает ли клик в заданный реф или не попадает.
Код return () => {...} сработает при демонтаже компонента, во избежании утечек памяти, слушатель удаляется.
В конце в квадратных скобочках указаны зависимости [ref, handler], при обновлении рефа обновится так же и слушатель.
src/hooks/onClickOutside.js*/
import { useEffect } from 'react';

const useOnClickOutside = (ref, handler) => {
  useEffect(() => {
    const listener = event => {
      if (!ref.current || ref.current.contains(event.target)) {
        return;
      }
      handler(event);
    };
    document.addEventListener('mousedown', listener);
    return () => {
      document.removeEventListener('mousedown', listener);
    };
  }, [ref, handler]);
};

export default useOnClickOutside;