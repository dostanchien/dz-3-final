//import {useLocation} from  "react-router-dom";
import PropTypes from 'prop-types';
import CardListOrder from "../../components/Card/Components/CardListOrder";

const CartPage = ({favorite, date, handleModal, handleCurrentPost, onClickIcon}) => {
	//const location = useLocation() /* useLocation хук нам дає обьект в якому індефикується кожна сторінка   */
	//console.log('HomePage location',location);
	return (
		<>

			<h2>Кошик</h2>
			<CardListOrder favorite={favorite} date={date} handleModal={handleModal} handleCurrentPost={handleCurrentPost}
				onClickIcon={onClickIcon}></CardListOrder>

		</>
	)
}

CartPage.defaultProps = {
	onClickIcon: () => {},
	handleModal: () => {},
	handleCurrentPost: () => {}
  }
  CartPage.propTypes = {
	  favorite: PropTypes.array,
	  date: PropTypes.array,
	handleModal: PropTypes.func,
	handleCurrentPost: PropTypes.func,
	onClickIcon: PropTypes.func,
	children:PropTypes.any
  }
export default CartPage